import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class CalculatorTesterfile {

	@Test
	void addTest() {
		Calculator calc = new Calculator();
		assertEquals(13, calc.add(4, 9));
		assertEquals(15, calc.add(7, 8));
	}

	void subtractTest() {
		Calculator calc = new Calculator();
		assertEquals(4, calc.subtract(6, 2));
		assertEquals(11, calc.subtract(19, 8));
	}
	
	void multiplyTest() {
		Calculator calc = new Calculator();
		assertEquals(36, calc.multiply(4, 9));
		assertEquals(56, calc.multiply(7, 8));
	}
	
	void divideTest() {
		Calculator calc = new Calculator();
		assertEquals(4, calc.divide(8, 2));
		assertEquals(3, calc.divide(9, 3));
	}
	
}
